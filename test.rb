require 'capybara/dsl'
require "selenium-webdriver"

Capybara.register_driver :selenium do |app|
Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

Capybara.register_driver :headless_chrome do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: { args: %w(headless disable-gpu) }
  )

  Capybara::Selenium::Driver.new app,
    browser: :chrome,
    desired_capabilities: capabilities
end

Capybara.default_driver = :headless_chrome


# Capybara.default_driver = :selenium

include Capybara::DSL

puts "started"
visit "http://bit.ly/2ATW8ZD"
puts "visited"
find('#ThrottleDropdown').find(:xpath, 'option[2]').select_option
puts "select box updated"
1600.times do |i|
  print '.'
  puts "100 secs complete" if i % 100 == 0
  sleep 1
end